# qt5_000_test_ui.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from PyQt5 import QtCore, uic
from PyQt5.QtWidgets import QApplication, QMainWindow

from .ui.testui import Ui_MainWindow as Test_Ui_MainWindow


class TestMainWindow(QMainWindow):
    def __init__(self):
        super(TestMainWindow, self).__init__()
        self.setupUi()

    def setup_ui(self):
        """
        Initialize the window and display its contents to the screen
        """

        uic.loadUi("pyqt5sample/ui/testui.ui", self)
        self.show()


select_items = {
    1: "China",
    2: "USA",
    3: "Russia",
    4: "India"
}


class UITestMainUIWindow(Test_Ui_MainWindow):
    def setup_ui(self, main_window: QMainWindow) -> None:
        super().setupUi(main_window)

        self.actionExit.triggered.connect(main_window.close)

        self._retranslate_ui(main_window)
        # QtCore.QMetaObject.connectSlotsByName(main_window)

    def _retranslate_ui(self, main_window: QMainWindow) -> None:
        _translate = QtCore.QCoreApplication.translate
        main_window.setWindowTitle(_translate("MainWindow", "New Main Window"))
        for k, v in select_items.items():
            self.comboBox.addItem("")
            self.comboBox.setItemText(k, _translate("MainWindow", v))


def run_test_ui() -> None:
    app = QApplication(sys.argv)
    main_window = QMainWindow()
    ui = UITestMainUIWindow()
    ui.setup_ui(main_window)
    main_window.show()

    sys.exit(app.exec_())
