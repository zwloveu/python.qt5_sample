# qt5_001_empty_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QWidget


class UIEmptyWindow():
    def __init__(self) -> None:
        pass

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 400, 300))

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "Empty Window in PyQt"))


def run_empty_window() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UIEmptyWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
