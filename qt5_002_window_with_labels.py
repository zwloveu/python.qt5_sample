# qt5_002_window_with_labels.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-


import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication, QWidget


class UIHelloWorldWindow():
    def __init__(self):
        pass

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 250, 250))

        qt5.set_label(widget, ax=105, ay=15, text="Hello")
        qt5.set_label(
            widget, image_file="pyqt5sample/assets/world.png", ax=25, ay=40)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "QLabels In Window"))


def run_window_with_labels() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UIHelloWorldWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
