# qt5_003_button_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QWidget


class UIButtonWindow():
    def __init__(self) -> None:
        pass

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 250, 150))
        qt5.set_label(widget, ax=60, ay=30, text="Don't push the button.")
        qt5.set_qpushbuttn(widget, ax=80, ay=70, text="Push Me").clicked.connect(
            lambda: self._buttonClicked(widget))

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "QPushButton Widget"))

    def _buttonClicked(self, widget: QWidget) -> None:
        """
        Print message to the terminal,
        and close the window when button is clicked.
        """

        print("The window has been closed.")
        widget.close()


def run_button_window() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UIButtonWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
