# qt5_004_checkbox_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QWidget


class UICheckBoxWindow():
    def __init__(self) -> None:
        pass

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 250, 250))

        qt5.set_label(widget, ax=10, ay=10, w=230, h=60,
                      text="Which shifts can you work? (Please check all that apply)",
                      wrap=True)
        morning_cb = qt5.set_qcheckbox(
            widget, text="Morning [8 AM - 2 PM]", ax=20, ay=80)
        morning_cb.stateChanged.connect(
            lambda: self._printToTerminal(widget, morning_cb.checkState()))
        afternoon_cb = qt5.set_qcheckbox(
            widget, text="Afternoon [1 PM - 8 PM]", ax=20, ay=100)
        afternoon_cb.stateChanged.connect(
            lambda: self._printToTerminal(widget, afternoon_cb.checkState()))
        night_cb = qt5.set_qcheckbox(
            widget, text="Night [7 PM - 3 AM]", ax=20, ay=120)
        night_cb.stateChanged.connect(
            lambda: self._printToTerminal(widget, night_cb.checkState()))
        self.check_result = qt5.set_label(widget, ax=10, ay=150, w=230, h=60)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "QCheckbox Widget"))

    def _printToTerminal(self, widget: QWidget, state: int) -> None:
        """
        Simple function to show how to determine the state of a checkbox.

        Prints the text label of the checkbox by determing which widget is sending the signal.
        """

        sender = widget.sender()
        if state == QtCore.Qt.Checked:
            self.check_result.setText(f"{sender.text()} Selected.")
        else:
            self.check_result.setText(f"{sender.text()} Deselected.")


def run_checkbox_window() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UICheckBoxWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
