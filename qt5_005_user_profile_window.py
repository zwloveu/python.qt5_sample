# qt5_005_user_profile_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication, QWidget


class UIUserProfileWindow():
    def __init__(self) -> None:
        pass

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen.
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(50, 50, 250, 500))

        qt5.set_label(widget, image_file="pyqt5sample/assets/skyblue.png")
        qt5.set_label(
            widget, image_file="pyqt5sample/assets/profile_image.png", ax=80, ay=20)
        qt5.set_label(widget, ax=75, ay=140, text="John Doe",
                      font=QFont("Arial", 20))
        qt5.set_label(widget, ax=1, ay=170, text="Biography",
                      font=QFont("Arial", 17))
        qt5.set_label(widget, ax=1, ay=200, text="I'm a Software Engineer with 8 years\
                                        experience creating awesome code",
                      wrap=True)
        qt5.set_label(widget, ax=1, ay=240, text="Skills",
                      font=QFont("Arial", 17))
        qt5.set_label(widget, ax=1, ay=270,
                      text="Python | PHP | SQL | JavaScript")
        qt5.set_label(widget, ax=1, ay=290, text="Experience",
                      font=QFont("Arial", 17))
        qt5.set_label(widget, ax=1, ay=320, text="Python Developer")
        qt5.set_label(widget, ax=1, ay=340, text="Mar 2011 - Present",
                      font=QFont("Arial", 10))
        qt5.set_label(widget, ax=1, ay=360, text="Pizza Delivery Driver")
        qt5.set_label(widget, ax=1, ay=380, text="Aug 2015 - Dec 2017",
                      font=QFont("Arial", 10))

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "User Profile"))


def run_user_profile_window() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UIUserProfileWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
