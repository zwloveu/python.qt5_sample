# qt5_006_line_edit_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QWidget


class UIQLineEditWindow():
    def __init__(self) -> None:
        pass

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """
        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 400, 200))

        qt5.set_label(widget, ax=100, ay=10,
                      text="Pleae enter your name below.")
        qt5.set_label(widget, ax=70, ay=50, text="Name:")
        self.name_entry = qt5.set_qlineedit(
            widget, ax=130, ay=50, w=200, h=20)
        qt5.set_qpushbuttn(widget, ax=160, ay=110, text="Clear").clicked.connect(
            lambda: self._clearEntries(widget))

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "QLineEdit Widget"))

    def _clearEntries(self, widget: QWidget) -> None:
        """
        If button is pressed clear the line edit input field.
        """

        sender = widget.sender()
        if sender.text() == "Clear":
            self.name_entry.clear()


def run_line_edit_window() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UIQLineEditWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
