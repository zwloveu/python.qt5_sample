# qt5_007_message_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import file as myfile
from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication, QMessageBox, QWidget


class AuthorData:
    def __init__(self) -> None:
        self.authors = []
        self._prepare_data()

    def _prepare_data(self) -> None:
        for author in myfile.read_text("pyqt5sample/assets/authors.txt"):
            self.authors.append(author.removesuffix("\n"))

    def find_author(self, author) -> bool:
        if author in self.authors:
            return True
        else:
            return False


class UIDisplayMessageBox():
    def __init__(self) -> None:
        self._author_data = AuthorData()

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 400, 200))

        qt5.set_label(widget, ax=20, ay=20,
                      text="Author Catalogue", font=QFont("Arial", 20))
        qt5.set_label(widget, ax=40, ay=60,
                      text="Enter the name of the author your are searching for:")
        qt5.set_label(widget, ax=50, ay=90, text="Name:")
        self.auth_entry = qt5.set_qlineedit(widget, ax=95, ay=90, w=240, h=20,
                                            place_holder="firstname lastname")
        qt5.set_qpushbuttn(widget, ax=125, ay=130, w=150, h=40,
                           text="Search").clicked.connect(lambda: self._displayMessageBox(widget))

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "QMessageBox Window"))

    def _displayMessageBox(self, widget: QWidget) -> None:
        """
        When button is clicked, search through catalogue of names.

        If name is found, display Author Found Dialog.

        Otherwise, display Author Not Found dialog.
        """

        author_found_msg_box = QMessageBox()
        if self._author_data.find_author(self.auth_entry.text().strip()):
            author_found_msg_box = QMessageBox.information(
                widget, "Author Found", f"Author {self.auth_entry.text()} found in catalogue", QMessageBox.Ok, QMessageBox.Ok)
        else:
            author_found_msg_box = QMessageBox.question(
                widget, "Author Not Found", "Author not found in catalogue.\nDo you wish to continue?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if author_found_msg_box == QMessageBox.No:
            widget.close()
        else:
            pass


def run_message_window() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UIDisplayMessageBox()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
