# qt5_008_login_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import file as myfile
from plupkg import qt5
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication, QMessageBox, QWidget


class UserData:
    def __init__(self) -> None:
        self.users: dict[str, str] = {}
        self.load_data()

    def load_data(self) -> None:
        for line in myfile.read_text("pyqt5sample/assets/users.txt"):
            user_fields: list[str] = line.split(" ")
            self.users[user_fields[0]] = user_fields[1].strip("\n")

    def find_user(self, username: str, password: str) -> bool:
        if (username, password) in self.users.items():
            return True
        else:
            return False

    def add_user(self, username: str, password: str, confirm_password: str) -> None:
        if password == confirm_password:
            myfile.write_text([username + " " + password],
                              "pyqt5sample/assets/users.txt", "a+", newline="\n")


class UICreateNewUser:
    def __init__(self) -> None:
        self._user_data = UserData()

    def show(self) -> None:
        window = QWidget()
        self.setup_ui(window)
        window.show()

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen.
        """

        widget.setObjectName("CreateNewUserWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 400, 400))

        self._setup_create_new_user_ui(widget)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate(
            "CreateNewUserWindow", "Create New User"))

    def _setup_create_new_user_ui(self, widget: QWidget) -> None:
        """
        Create widgets that will be used to collect information
        from the user to create a new account. 
        """

        # create label for image
        qt5.set_label(
            widget, image_file="pyqt5sample/assets/new_user_icon.png", ax=150, ay=60)

        qt5.set_label(widget, text="create new account",
                      ax=110, ay=20, font=QFont('Arial', 20))

        # username and fullname labels and line edit widgets
        qt5.set_label(widget, text="username:", ax=50, ay=180)
        self.name_entry = qt5.set_qlineedit(
            widget, ax=130, ay=180, w=200, h=20)
        qt5.set_label(widget, text="full name:", ax=50, ay=210)
        qt5.set_qlineedit(widget, ax=130, ay=210, w=200, h=20)

        # create password and confirm password labels and line edit widgets
        qt5.set_label(widget, text="password:", ax=50, ay=240)
        self.pswd_entry = qt5.set_qlineedit(
            widget, ax=130, ay=240, w=200, h=20)
        self.pswd_entry.setEchoMode(QtWidgets.QLineEdit.Password)
        qt5.set_label(widget, text="confirm:", ax=50, ay=270)
        self.confirm_entry = qt5.set_qlineedit(
            widget, ax=130, ay=270, w=200, h=20)
        self.confirm_entry.setEchoMode(QtWidgets.QLineEdit.Password)

        # create sign up button
        qt5.set_qpushbuttn(widget, text="sign up", ax=100, ay=310, w=200, h=40).clicked.connect(
            lambda: self._confirm_signup(widget))

    def _confirm_signup(self, widget: QWidget) -> None:
        """
        When user presses sign up, check if the passwords match.
        If they match, then save username and password text to users.txt.
        """

        user_name = self.name_entry.text().strip()
        password = self.pswd_entry.text().strip()
        confirm_password = self.confirm_entry.text().strip()

        if password != confirm_password:
            # display messagebox if passwords don't match
            QMessageBox.warning(widget, "Error Message",
                                "The passwords you entered do not match. Please try again.", QMessageBox.Close,
                                QMessageBox.Close)
        else:
            # if passwords match, save passwords to file and return to login
            # and test if you can login with new user information.
            self._user_data.add_user(user_name, password, confirm_password)
            widget.close()


class UILoginWindow():
    def __init__(self) -> None:
        self._user_data = UserData()

    def show(self) -> None:
        window = QWidget()
        self.setup_ui(window)
        window.show()

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen.
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(50, 50, 370, 500))

        self._setup_login_ui(widget)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _setup_login_ui(self, widget: QWidget) -> None:
        """
        Create the login ui.
        """

        qt5.set_label(widget, text="login", ax=180,
                      ay=10, font=QFont("Arial", 20))

        # Username and password labels and line edit widgets
        qt5.set_label(widget, text="username:", ax=30, ay=60)
        self.name_entry = qt5.set_qlineedit(widget, ax=110, ay=60, w=220, h=20)

        qt5.set_label(widget, text="password:", ax=30, ay=90)
        self.pwd_entry = qt5.set_qlineedit(widget, ax=110, ay=90, w=220, h=20)
        self.pwd_entry.setEchoMode(QtWidgets.QLineEdit.Password)

        # Sign in push button
        qt5.set_qpushbuttn(widget, text="login", ax=100, ay=140,
                           w=200, h=40).clicked.connect(lambda: self._login(widget))

        # Display show password checkbox
        pwd_cb = qt5.set_qcheckbox(
            widget, text="show password", ax=110, ay=115)
        pwd_cb.toggle()
        pwd_cb.setChecked(False)
        pwd_cb.stateChanged.connect(self._show_password)

        # Display sign up label and push button
        qt5.set_label(widget, text="not a member?", ax=70, ay=200)
        qt5.set_qpushbuttn(widget, text="sign up", ax=180, ay=195).clicked.connect(
            self._create_new_user)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "Login Window"))

    def _login(self, widget: QWidget) -> None:
        """
        When user clicks sign in button, check if username and password
        match any existing profiles in the user.txt

        If they exist, display messagebox and close program.

        If the don't, display error messagebox.
        """

        self._user_data.load_data()
        user_name = self.name_entry.text().strip()
        password = self.pwd_entry.text().strip()

        if self._user_data.find_user(user_name, password):
            QMessageBox.information(
                widget, "Login Successful!", "Login Successful!", QMessageBox.Ok, QMessageBox.Ok)
            widget.close()
        else:
            QMessageBox.warning(
                widget, "Error Message", "The username or password is incorrect.", QMessageBox.Close, QMessageBox.Close)

    def _show_password(self, state: int) -> None:
        """
        If checkbox is enabled, view password.

        Else, mask password so other cannot see it.
        """

        if state == QtCore.Qt.Checked:
            self.pwd_entry.setEchoMode(QtWidgets.QLineEdit.Normal)
        else:
            self.pwd_entry.setEchoMode(QtWidgets.QLineEdit.Password)

    def _create_new_user(self) -> None:
        UICreateNewUser().show()


def run_login_window() -> None:
    app = QApplication(sys.argv)
    UILoginWindow().show()
    sys.exit(app.exec_())
