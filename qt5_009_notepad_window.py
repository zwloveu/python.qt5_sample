# qt5_009_notepad_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-


import sys

from plupkg import file as myfile
from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication, QFileDialog, QMessageBox, QWidget


class UINotepadWindow:
    def __init__(self) -> None:
        pass

    def show(self) -> None:
        widget = QWidget()
        self.setup_ui(widget)
        widget.show()

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 300, 400))

        self._setup_notepad_ui(widget)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "Notepad Window"))

    def _setup_notepad_ui(self, widget: QWidget) -> None:
        # Create push buttons for editing menu
        qt5.set_qpushbuttn(widget, text="New", ax=10,
                           ay=20).clicked.connect(lambda: self._clear_text(widget))
        qt5.set_qpushbuttn(widget, text="Save", ax=80,
                           ay=20).clicked.connect(lambda: self._save_text(widget))

        # Create text edit field
        self.text_field = qt5.set_qtextedit(widget, ax=10, ay=60, w=280, h=330)

    def _clear_text(self, widget: QWidget) -> None:
        """
        If the new button is clicked, display dialog asking user if 
        they want to clear the text edid field or not
        """

        answer = QMessageBox.question(widget, "Clear Text",
                                      "Do you want to clear the text?", QMessageBox.No | QMessageBox.Yes,
                                      QMessageBox.Yes)
        if answer == QMessageBox.Yes:
            self.text_field.clear()
        else:
            pass

    def _save_text(self, widget: QWidget) -> None:
        """
        If the save button is clicked, display dialog to save the text in
        the text edit field to a text file.
        """

        notepad_text = self.text_field.toPlainText()
        file_name = qt5.save_file_dialog(widget)
        if file_name:
            myfile.write_text([notepad_text], file_name, "w")


def run_notepad_window() -> None:
    app = QApplication(sys.argv)
    UINotepadWindow().show()
    sys.exit(app.exec_())
