# qt5_010_survey_with_boxlayout_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import (QApplication, QButtonGroup, QHBoxLayout,
                             QVBoxLayout, QWidget)


class UISurveyWithBoxLayoutWindow:
    def __init__(self) -> None:
        pass

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 400, 230))

        self._setup_survey_ui(widget)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "Survey Window"))

    def _setup_survey_ui(self, widget: QWidget) -> None:
        # Set main laytout of the window
        v_box = QVBoxLayout()
        widget.setLayout(v_box)

        # Set title layout
        title_h_box = QHBoxLayout()
        v_box.addLayout(title_h_box)
        v_box.addStretch(1)

        # Set question layout
        q_h_box = QHBoxLayout()
        v_box.addLayout(q_h_box)
        v_box.addStretch(2)

        # Set rating layout
        ratings_h_box = QHBoxLayout()
        ratings_h_box.setSpacing(60)
        cb_h_box = QHBoxLayout()
        cb_h_box.setSpacing(100)
        v_box.addLayout(ratings_h_box)
        v_box.addLayout(cb_h_box)
        v_box.addStretch(3)

        # Set button layout
        btn_h_box = QHBoxLayout()
        v_box.addLayout(btn_h_box)
        v_box.addStretch(4)

        # Add title label to title_h_box
        title = qt5.set_label(text="Restaurant Name", font=QFont("Arial", 17))
        title_h_box.addStretch()
        title_h_box.addWidget(title)
        title_h_box.addStretch()

        # Add question label to v_box
        question = qt5.set_label(text="How would you rate your service today?")
        q_h_box.addStretch()
        q_h_box.addWidget(question)
        q_h_box.addStretch()

        # Set rating labels and checkboxes
        ratings = ["Not Satisfied", "Average", "Satisfied"]
        ratings_h_box.addStretch()
        cb_h_box.addStretch()
        scale_bg = QButtonGroup(widget)
        scale_bg.buttonClicked.connect(self._cb_clicked)

        for i in range(len(ratings)):
            rate_label = qt5.set_label(text=ratings[i])
            ratings_h_box.addWidget(rate_label)

            scale_cb = qt5.set_qcheckbox(text=str(i))
            if i == 1:
                scale_cb.setChecked(True)
            cb_h_box.addWidget(scale_cb)
            scale_bg.addButton(scale_cb)
        ratings_h_box.addStretch()
        cb_h_box.addStretch()

        # Add close button to btn_h_box
        close_btn = qt5.set_qpushbuttn(text="Close")
        close_btn.clicked.connect(widget.close)
        btn_h_box.addStretch()
        btn_h_box.addWidget(close_btn)
        btn_h_box.addStretch()

    def _cb_clicked(self, cb) -> None:
        print(f"{cb.text()} Selected")


def run_survey_with_boxlayout_window() -> None:
    app = QApplication(sys.argv)
    widget = QWidget()
    ui = UISurveyWithBoxLayoutWindow()
    ui.setup_ui(widget)
    widget.show()
    sys.exit(app.exec_())
