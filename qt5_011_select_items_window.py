# qt5_011_select_items_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QVBoxLayout, QWidget


class UISelectItemsWindow:
    def __init__(self) -> None:
        self.lunch_list = ["egg", "turkey sandwich", "ham sandwich", "cheese",
                           "hummus", "yogurt", "apple", "banana", "orange", "waffle",
                           "baby carrots", "bread", "pasta", "crackers", "pretzels",
                           "pita chips", "coffee", "soda", "water"]

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 400, 200))

        self._setup_inner_ui(widget)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "ComboBox And SpinBox"))

    def _setup_inner_ui(self, widget: QWidget) -> None:
        # Set main layout of the window
        v_box = QVBoxLayout()
        widget.setLayout(v_box)

        # Set header layout
        header_h_box = QHBoxLayout()
        v_box.addLayout(header_h_box)
        v_box.addStretch(1)

        # Set content layouts
        h_box1 = QHBoxLayout()
        h_box2 = QHBoxLayout()
        v_box.addLayout(h_box1)
        v_box.addLayout(h_box2)
        v_box.addStretch(1)

        # Set footer layout
        footer_h_box = QHBoxLayout()
        v_box.addLayout(footer_h_box)
        v_box.addStretch(1)

        # Set contents for header layout
        header_h_box.addWidget(qt5.set_label(text="Select 2 items you had for lunch and their prices.", font=QFont(
            "Arial", 16), alignment=Qt.AlignCenter))

        # Set contents for content layouts
        h_box1.addStretch()
        h_box1.addWidget(qt5.set_qcombobox(texts=self.lunch_list))
        h_box1.addStretch()
        self.price_sb1 = qt5.set_qspinbox(r_min=0, r_max=100, prefix="$")
        self.price_sb1.valueChanged.connect(self._calculate_total)
        h_box1.addWidget(self.price_sb1)
        h_box1.addStretch()

        h_box2.addStretch()
        h_box2.addWidget(qt5.set_qcombobox(texts=self.lunch_list))
        h_box2.addStretch()
        self.price_sb2 = qt5.set_qspinbox(r_min=0, r_max=100, prefix="$")
        self.price_sb2.valueChanged.connect(self._calculate_total)
        h_box2.addWidget(self.price_sb2)
        h_box2.addStretch()

        # Set contents for footer layout
        self.total_lb = qt5.set_label(text="Total Spent: $", font=QFont(
            "Arial", 16), alignment=Qt.AlignRight)
        footer_h_box.addWidget(self.total_lb)

    def _calculate_total(self) -> None:
        total = self.price_sb1.value() + self.price_sb2.value()
        self.total_lb.setText(f"Total Spent: ${format(str(total))}")


def run_select_items_window() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UISelectItemsWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
