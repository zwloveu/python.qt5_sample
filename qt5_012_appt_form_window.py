# qt5_012_appt_form_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import (QApplication, QFormLayout, QHBoxLayout, QWidget)


class UIApptFormWindow:
    def __init__(self) -> None:
        pass

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 300, 400))

        self._setup_inner_ui(widget)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate(
            "MainWindow", "Application Form Window"))

    def _setup_inner_ui(self, widget: QWidget) -> None:
        # Set main layout of the window
        app_form_layout = QFormLayout()
        app_form_layout.setLabelAlignment(Qt.AlignRight)
        widget.setLayout(app_form_layout)

        app_form_layout.addRow(qt5.set_label(text="Appointment Submission Form", font=QFont(
            'Arial', 18), alignment=Qt.AlignCenter))
        app_form_layout.addRow("Full Name", qt5.set_qlineedit(w=100, h=100))
        app_form_layout.addRow("Address", qt5.set_qlineedit())
        app_form_layout.addRow(
            "Mobile Number", qt5.set_qlineedit(input_mask="000-000-0000"))

        h_box = QHBoxLayout()
        h_box.addSpacing(10)
        app_form_layout.addRow(h_box)
        h_box.addStretch()
        h_box.addWidget(qt5.set_label(text="Age"))
        h_box.addWidget(qt5.set_qspinbox(r_min=1, r_max=110))
        h_box.addStretch()
        h_box.addWidget(qt5.set_label(text="Height"))
        h_box.addWidget(qt5.set_qlineedit(place_holder="cm"))
        h_box.addStretch()
        h_box.addWidget(qt5.set_label(text="Weight"))
        h_box.addWidget(qt5.set_qlineedit(place_holder="kg"))
        h_box.addStretch()

        app_form_layout.addRow(
            "Gender", qt5.set_qcombobox(texts=["Male", "Female"]))
        app_form_layout.addRow("Past Surgeries ", qt5.set_qtextedit(
            place_holder="separate by ','"))
        app_form_layout.addRow(
            "Blood Type", qt5.set_qcombobox(texts=["A", "B", "AB", "O"]))

        desired_time_h_box = QHBoxLayout()
        desired_time_h_box.addSpacing(10)
        app_form_layout.addRow("Desired Time", desired_time_h_box)
        desired_time_h_box.addStretch()
        desired_time_h_box.addWidget(qt5.set_qspinbox(r_min=1, r_max=12))
        desired_time_h_box.addStretch()
        desired_time_h_box.addWidget(qt5.set_qcombobox(
            texts=[":00", ":15", ":30", ":45"]))
        desired_time_h_box.addStretch()
        desired_time_h_box.addWidget(qt5.set_qcombobox(texts=["AM", "PM"]))
        desired_time_h_box.addStretch()

        submit_h_box = QHBoxLayout()
        submit_h_box.addSpacing(30)
        app_form_layout.addRow(submit_h_box)
        submit_h_box.addStretch()
        submit_button = qt5.set_qpushbuttn(text="Submit Appointment")
        submit_h_box.addWidget(submit_button)
        submit_h_box.addStretch()
        submit_button.clicked.connect(widget.close)


def run_appt_form_window() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UIApptFormWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
