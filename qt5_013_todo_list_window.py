# qt5_013_todo_list_window.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import (QApplication, QGridLayout, QHBoxLayout,
                             QVBoxLayout, QWidget)


class UIToDoListWindow:
    def __init__(self) -> None:
        self.max_must_do_count: int = 15

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 500, 350))

        self._setup_inner_ui(widget)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate(
            "MainWindow", "To Do List Window"))

    def _setup_inner_ui(self, widget: QWidget) -> None:
        # Set main layout of the window
        todo_grid_layout = QGridLayout()
        widget.setLayout(todo_grid_layout)

        # Add title -> row:0 column:0 rowspan:1 columnspan:2
        todo_grid_layout.addWidget(qt5.set_label(text="To Do List", font=QFont(
            "Arial", 24), alignment=Qt.AlignCenter), 0, 0, 1, 2)

        # Add Must Dos and Appointments -> row:1
        must_do_h_box_layout = QHBoxLayout()
        todo_grid_layout.addLayout(must_do_h_box_layout, 1, 0)
        must_do_h_box_layout.addStretch()
        must_do_h_box_layout.addWidget(qt5.set_label(text="Must Dos", font=QFont(
            'Arial', 20), alignment=Qt.AlignCenter))
        must_do_h_box_layout.addStretch()
        add_must_do_btn = qt5.set_qpushbuttn(text="+")
        add_must_do_btn.clicked.connect(self._add_must_do)
        must_do_h_box_layout.addWidget(add_must_do_btn)
        must_do_h_box_layout.addStretch()
        todo_grid_layout.addWidget(qt5.set_label(
            text="Appointments", font=QFont("Arial", 20), alignment=Qt.AlignCenter), 1, 1)

        # Set Must Dos section layout -> row:2 column 0
        self.mustdo_grid = QGridLayout()
        self.mustdo_grid.setContentsMargins(5, 5, 5, 5)
        todo_grid_layout.addLayout(self.mustdo_grid, 2, 0)

        # Set Appointments section layout -> row:2 column 1
        appt_v_box = QVBoxLayout()
        appt_v_box.setContentsMargins(5, 5, 5, 5)
        todo_grid_layout.addLayout(appt_v_box, 2, 1)

        # Fill Must Dos section
        for position in range(0, self.max_must_do_count):
            self.mustdo_grid.addWidget(
                qt5.set_qcheckbox(checked=False), position, 0)
            self.mustdo_grid.addWidget(
                qt5.set_qlineedit(min_width=200), position, 1)

        # Fill Appointments section
        appt_v_box.addWidget(qt5.set_label(
            text="Morning", font=QFont("Arial", 16)))
        appt_v_box.addWidget(qt5.set_qtextedit())
        appt_v_box.addWidget(qt5.set_label(
            text="Noon", font=QFont("Arial", 16)))
        appt_v_box.addWidget(qt5.set_qtextedit())
        appt_v_box.addWidget(qt5.set_label(
            text="Evening", font=QFont("Arial", 16)))
        appt_v_box.addWidget(qt5.set_qtextedit())

        # Add close button -> row:3 column:0 rowspan:1 columnspan:2
        close_button = qt5.set_qpushbuttn(text="Close")
        close_button.clicked.connect(widget.close)
        todo_grid_layout.addWidget(close_button, 3, 0, 1, 2)

    def _add_must_do(self):
        self.max_must_do_count += 1
        self.mustdo_grid.addWidget(
            qt5.set_qcheckbox(checked=False), self.max_must_do_count, 0)
        self.mustdo_grid.addWidget(
            qt5.set_qlineedit(min_width=200), self.max_must_do_count, 1)


def run_todo_list_window() -> None:
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UIToDoListWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())
