# qt5_101_web_browser.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import os
import sys

from plupkg import qt5
from PyQt5 import QtCore
from PyQt5.QtCore import QSize, Qt, QUrl
from PyQt5.QtGui import QFont, QIcon
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import (QApplication, QDesktopWidget, QLineEdit,
                             QMainWindow, QStatusBar, QTabWidget, QVBoxLayout,
                             QWidget)

style_sheet = """
    QTabWidget:pane{
        border: none
    }
"""


class UIWebBrowserWindow(QMainWindow):
    def __init__(self) -> None:
        super().__init__()
        # lists that will keep track of the new windows/tabs/urls
        self.window_list = []
        self.web_pages_list: list[QWebEngineView] = []
        self.urls_list: list[QLineEdit] = []
        self.home_url = "https:/www.qq.com"

        self.setup_ui()

    def setup_ui(self) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        self.setObjectName("MainWindow")
        self.setWindowTitle("Qt Web Browser")
        self.setWindowIcon(QIcon("pyqt5sample/assets/pyqt_logo.png"))
        self.setMinimumSize(300, 200)
        desktop = QDesktopWidget().screenGeometry()
        self.setGeometry(QtCore.QRect(
            0, 0, desktop.width(), desktop.height()))

        self._setup_inner_ui()

    def _setup_inner_ui(self) -> None:
        # Menu Bar
        menu_bar = self.menuBar()
        # menu_bar.setNativeMenuBar(False) # this line only works when using MacOS
        file_menu = menu_bar.addMenu("File")
        file_menu.addAction(qt5.set_qaction(
            parent=self,
            text="New Window", shortcut="Ctrl+N", func=self._open_new_window))
        file_menu.addAction(qt5.set_qaction(
            parent=self,
            text="New Tab", shortcut="Ctrl+T", func=self._open_new_tab))
        file_menu.addSeparator()
        file_menu.addAction(qt5.set_qaction(
            parent=self,
            text="Quit Browser", shortcut="Ctrl+Q", func=self.close))

        # Tool Bar
        tool_bar = qt5.set_tool_bar(
            title="Address Bar", icon_size=QSize(30, 30))
        self.addToolBar(tool_bar)
        tool_bar.addAction(
            qt5.set_qaction(
                parent=self, text="Home",
                icon_path="pyqt5sample/assets/home.png", func=self._goto_home))
        tool_bar.addAction(
            qt5.set_qaction(
                parent=self, text="Back",
                icon_path="pyqt5sample/assets/back.png", func=self._goto_back))
        tool_bar.addAction(
            qt5.set_qaction(
                parent=self, text="Forward",
                icon_path="pyqt5sample/assets/forward.png", func=self._goto_forward))
        tool_bar.addAction(
            qt5.set_qaction(
                parent=self, text="Refresh",
                icon_path="pyqt5sample/assets/refresh.png", func=self._refresh_page))
        self.address_line = qt5.set_qlineedit(
            parent=self,
            place_holder="Enter website address", func=self._search_for_url)
        self.address_line.addAction(
            QIcon("pyqt5sample/assets/search.png"), 0)  # 0=QLineEdit.LeadingPosition
        tool_bar.addWidget(self.address_line)
        tool_bar.addAction(
            qt5.set_qaction(
                parent=self, text="Stop",
                icon_path="pyqt5sample/assets/stop.png", func=self._stop_page))

        # Tabs
        """
        Create the QTabWidget object and the different pages.
        Handle when a tab is closed.
        """
        self.tab_bar = QTabWidget()
        # Add close buttons to tabs
        self.tab_bar.setTabsClosable(True)  
        # Hides tab bar when less than 2 tabs
        self.tab_bar.setTabBarAutoHide(False)
        self.tab_bar.tabCloseRequested.connect(self._close_tab)
        self.setCentralWidget(self.tab_bar)
        self._add_new_tab(True)
        # If user switches pages, update the URL in the address to
        # reflect the current page.        
        self.tab_bar.currentChanged.connect(self._update_url)

        # Status Bar
        self.page_load_pb = qt5.set_process_bar()
        self.page_load_label = qt5.set_label()
        self.status_bar = QStatusBar()
        self.setStatusBar(self.status_bar)

    def _add_new_tab(self, is_main: bool = False) -> None:
        new_tab = QWidget()
        self.tab_bar.addTab(new_tab, "New Tab")
        tab_v_box = QVBoxLayout()
        new_tab.setLayout(tab_v_box)

        """
        Create individual tabs and widgets. Add the tab's url and web view
        to the appropriate list.
        Update the address bar if the user switches tabs.
        """

        # Create the web view that will be displayed on the page.
        current_web_page = qt5.set_web_view(
            url=self.home_url,
            load_progress_func=self._update_progress_bar,
            url_changed_func=self._update_url,
            load_finished_func=self._update_tab_title)
        # Sets the left, top, right, and bottom margins to use around the layout.
        tab_v_box.setContentsMargins(0, 0, 0, 0)
        tab_v_box.addWidget(current_web_page)
        # Append new web_page and URL to the appropriate lists
        self.web_pages_list.append(current_web_page)
        self.urls_list.append(self.address_line)
        self.tab_bar.setCurrentWidget(current_web_page)

        # Update the tab_bar index to keep track of the new tab.
        # Load the URL for the new page.
        if not is_main:
            tab_index = self.tab_bar.currentIndex()
            self.tab_bar.setCurrentIndex(tab_index + 1)

    def _open_new_window(self) -> None:
        """
        Create new instance 
        """

        new_ui = UIWebBrowserWindow()
        new_ui.show()
        self.window_list.append(new_ui)

    def _open_new_tab(self) -> None:
        self._add_new_tab()

    def _goto_home(self) -> None:
        self.web_pages_list[self.tab_bar.currentIndex()].setUrl(
            QUrl(self.home_url))

    def _goto_back(self) -> None:
        pass

    def _goto_forward(self) -> None:
        pass

    def _refresh_page(self) -> None:
        self.web_pages_list[self.tab_bar.currentIndex()].reload()

    def _search_for_url(self) -> None:
        """
        Make a request to load a url.
        """

        url_text = self.urls_list[self.tab_bar.currentIndex()].text()
        # Append http to URL
        url = QUrl(url_text)
        if url.scheme() == "":
            url.setScheme("http")
        # Request URL
        if url.isValid():
            self.web_pages_list[self.tab_bar.currentIndex()].page().load(url)
        else:
            url.clear()

    def _stop_page(self) -> None:
        self.web_pages_list[self.tab_bar.currentIndex()].stop()

    def _close_tab(self, tab_index) -> None:
        """
        This signal is emitted when the close button on a tab is clicked.
        The index is the index of the tab that should be removed.
        """
        self.web_pages_list.pop(tab_index)
        self.urls_list.pop(tab_index)
        self.tab_bar.removeTab(tab_index)

    def _update_url(self) -> None:
        """
        Update the url in the address to reflect the current page being displayed.
        """

        url = self.web_pages_list[self.tab_bar.currentIndex()].page().url()
        formatted_url = QUrl(url).toString()
        self.urls_list[self.tab_bar.currentIndex()].setText(formatted_url)

    def _update_progress_bar(self, progress) -> None:
        """
        Update progress bar in status bar.
        This provides feedback to the user that the page is still loading.
        """
        if progress < 100:
            self.page_load_pb.setVisible(progress)
            self.page_load_pb.setValue(progress)
            self.page_load_label.setVisible(progress)
            self.page_load_label.setText(f"Loading Page... ({progress}/100)")
            self.status_bar.addWidget(self.page_load_pb)
            self.status_bar.addWidget(self.page_load_label)
        else:
            self.status_bar.removeWidget(self.page_load_pb)
            self.status_bar.removeWidget(self.page_load_label)

    def _update_tab_title(self) -> None:
        tab_index = self.tab_bar.currentIndex()
        title = self.web_pages_list[self.tab_bar.currentIndex()].page().title()
        self.tab_bar.setTabText(tab_index, title)


def run_web_browser() -> None:
    app = QApplication(sys.argv)
    ui = UIWebBrowserWindow()
    ui.show()
    sys.exit(app.exec_())
