# run_qt5.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

from qt5_000_test_ui import run_test_ui
from qt5_001_empty_window import run_empty_window
from qt5_002_window_with_labels import run_window_with_labels
from qt5_003_button_window import run_button_window
from qt5_004_checkbox_window import run_checkbox_window
from qt5_005_user_profile_window import run_user_profile_window
from qt5_006_line_edit_window import run_line_edit_window
from qt5_007_message_window import run_message_window
from qt5_008_login_window import run_login_window
from qt5_009_notepad_window import run_notepad_window
from qt5_010_survey_with_boxlayout_window import \
    run_survey_with_boxlayout_window
from qt5_011_select_items_window import run_select_items_window
from qt5_012_appt_form_window import run_appt_form_window
from qt5_013_todo_list_window import run_todo_list_window
from qt5_101_web_browser import run_web_browser

if __name__ == "__main__":
    # run_window_with_labels()
    # run_test_ui()
    # run_user_profile_window()
    # run_empty_window()
    # run_button_window()
    # run_line_edit_window()
    # run_checkbox_window()
    # run_message_window()
    # run_login_window()
    # run_notepad_window()
    # run_survey_with_boxlayout_window()
    # run_select_items_window()
    # run_appt_form_window()
    # run_todo_list_window()
    run_web_browser()
