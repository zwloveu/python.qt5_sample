# ui_sample.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import sys

from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QWidget


class UISampleWindow:
    def __init__(self) -> None:
        pass

    def setup_ui(self, widget: QWidget) -> None:
        """
        Initialize the window and display its contents to the screen
        """

        widget.setObjectName("MainWindow")
        widget.setGeometry(QtCore.QRect(100, 100, 400, 200))

        self._setup_inner_ui(widget)

        self._retranslate_ui(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def _retranslate_ui(self, widget: QWidget) -> None:
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("MainWindow", "UI Sample"))

    def _setup_inner_ui(self, widget: QWidget) -> None:
        pass


def run_sample_window():
    app = QApplication(sys.argv)
    window = QWidget()
    ui = UISampleWindow()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    run_sample_window()
